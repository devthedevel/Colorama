package net.dev909.colorama.data;

import java.util.List;

import net.dev909.colorama.data.blocks.BlockStainedPlanks;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.common.ForgeVersion;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class ColoramaEventHandler {

	/*
	@SubscribeEvent
	public void placeEvent(PlaceEvent event) {
		switch (Block.getIdFromBlock(event.state.getBlock())) {
		case 5:
			event.world.setBlockState(event.pos, ColoramaReg.blockStainedPlanks.getStateFromMeta(event.itemInHand.getMetadata()));
			break;
		case 45:
			event.world.setBlockState(event.pos, ColoramaReg.blockStainedBricks.getStateFromMeta(event.itemInHand.getMetadata()));
			break;
		case 98:
			event.world.setBlockState(event.pos, ColoramaReg.blockStainedStoneBricks.getStateFromMeta(event.itemInHand.getMetadata()));
			break;
		}

		/*
		 * if (block instanceof BlockPlanks) {
		 * event.world.setBlockState(event.pos,
		 * ColoramaReg.CPlanks.getStateFromMeta(event.itemInHand.getMetadata()))
		 * ; }
		 

	}
*/
	@SubscribeEvent
	public void harvestEvent(HarvestDropsEvent event) {
		if (event.state.getBlock() instanceof BlockStainedPlanks) {
			event.drops.iterator().next().setItem(Item.getItemFromBlock(Blocks.planks.getStateFromMeta(Block.getStateId(event.state)).getBlock()));
		}
		/*
		 * if (block instanceof BlockStainedPlanks) { event.drops.clear();
		 * event.drops.add(new ItemStack(Blocks.planks, 1,
		 * block.getMetaFromState(event.state))); }
		 * 
		 * if (block instanceof BlockStainedStoneBrick) { event.drops.clear();
		 * event.drops.add(new ItemStack(Blocks.stonebrick, 1,
		 * block.getMetaFromState(event.state))); }
		 */
	}
	
	@SubscribeEvent
	public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
		MinecraftServer.getServer().getCommandManager().executeCommand(event.player, "paintbook");
		List<ModContainer> list = Loader.instance().getActiveModList();
		for (ModContainer m : list) {
			if (m.getModId().equalsIgnoreCase(ColoramaMod.MODID)) {
				if (ForgeVersion.getResult(m).status.equals(ForgeVersion.Status.OUTDATED)){
					event.player.addChatMessage(new ChatComponentText("[COLORAMA!] New version available! Check the \"Mods\" menu for details!"));	
				}
				else {
					//event.player.addChatMessage(new ChatComponentText("Colorama up to date!"));
				}
				break;
			}
		}
	}
}
