package net.dev909.colorama.data.items;

import net.dev909.colorama.data.ColoramaReg;
import net.minecraft.item.Item;

public class ItemPaintcanEmpty extends Item {
	
	public ItemPaintcanEmpty(String name) {
		this.setMaxDamage(0);
		this.setHasSubtypes(false);
		this.setMaxStackSize(64);
		this.setUnlocalizedName(name);
		this.setCreativeTab(ColoramaReg.cTab);
	}
}
