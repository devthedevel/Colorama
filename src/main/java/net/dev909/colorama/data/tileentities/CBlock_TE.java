package net.dev909.colorama.data.tileentities;

import net.dev909.colorama.data.utils.cUtils;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import scala.actors.threadpool.Arrays;

public class CBlock_TE extends TileEntity {

	private static final int[] DEFAULT_RGB = { 238, 238, 238 };
	private final int[] RGB;

	public CBlock_TE(int[] rgb) {
		this.RGB = cUtils.Clone(rgb);
	}

	public CBlock_TE() {
		this.RGB = cUtils.Clone(DEFAULT_RGB);
	}
	public int getColor() {
		return cUtils.convertToInt(this.RGB);
	}

	public void setColor(int[] rgb) {
		cUtils.Clone(this.RGB, rgb);
	}

	public boolean setColor(NBTTagCompound NBT) {
		if (NBT.hasKey("rgb")) {
			this.setColor(NBT.getIntArray("rgb"));
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbtTagCompound = new NBTTagCompound();
		this.writeToNBT(nbtTagCompound);
		int metadata = this.getBlockMetadata();
		return new S35PacketUpdateTileEntity(this.pos, metadata, nbtTagCompound);
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
		this.readFromNBT(pkt.getNbtCompound());
		this.getWorld().markBlockForUpdate(pkt.getPos());
	}

	@Override
	public void writeToNBT(NBTTagCompound parentNBTTagCompound) {
		super.writeToNBT(parentNBTTagCompound);
		parentNBTTagCompound.setIntArray("rgb", this.RGB);
	}

	@Override
	public void readFromNBT(NBTTagCompound parentNBTTagCompound) {
		super.readFromNBT(parentNBTTagCompound);
		if (parentNBTTagCompound.hasKey("rgb")) {
			cUtils.Clone(RGB, parentNBTTagCompound.getIntArray("rgb"));
		} else
			cUtils.Clone(RGB, DEFAULT_RGB);
	}

}
