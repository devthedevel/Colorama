package net.dev909.colorama.data;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ColoramaTab extends CreativeTabs {

	public ColoramaTab(String label) {
		super(label);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Item getTabIconItem() {
		return ColoramaReg.paintbrush;
	}

}
