package net.dev909.colorama.data.commands;

import java.util.ArrayList;
import java.util.List;

import net.dev909.colorama.data.ColoramaReg;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;

public class CommandPaintbook implements ICommand {
	
	private final ArrayList<String> alias;
	
	public CommandPaintbook() {
		alias = new ArrayList<String>();
		alias.add("pbook");
		alias.add("paintb");
	}

	@Override
	public int compareTo(ICommand arg0) {
		return 0;
	}

	@Override
	public String getCommandName() {
		return "paintbook";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "paintbook";
	}

	@Override
	public List<String> getCommandAliases() {
		return this.alias;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		if (sender instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer)sender;
			player.inventory.addItemStackToInventory(new ItemStack(ColoramaReg.paintbook));
		}
	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}

	@Override
	public List<String> addTabCompletionOptions(ICommandSender sender, String[] args, BlockPos pos) {
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		return false;
	}
	
}