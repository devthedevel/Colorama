package net.dev909.colorama.data.blocks;

import java.util.List;
import java.util.Random;

import net.dev909.colorama.data.ColoramaReg;
import net.dev909.colorama.data.tileentities.CBlock_TE;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockStainedPlanks extends CBlock {

	   public static final PropertyEnum<EnumType> VARIANT = PropertyEnum.create("variant", EnumType.class);

	    public BlockStainedPlanks(String name) {
	        super(Material.wood, name);
			this.setHardness(2.0F);
			this.setResistance(5.0F);
			this.setStepSound(soundTypeWood);
	        this.setDefaultState(this.blockState.getBaseState().withProperty(VARIANT, EnumType.OAK));
	        this.setCreativeTab(ColoramaReg.cTab);
	    }

		@Override
		public boolean isFlammable(IBlockAccess world, BlockPos pos, EnumFacing face) {
			return true;
		}
	    
	    /**
	     * Gets the metadata of the item this Block can drop. This method is called when the block gets destroyed. It
	     * returns the metadata of the dropped item based on the old metadata of the block.
	     */	    
		@Override
	    public int damageDropped(IBlockState state) {
	        return ((EnumType)state.getValue(VARIANT)).getMetadata();
	    }

	    /**
	     * returns a list of blocks with the same ID, but different meta (eg: wood returns 4 blocks)
	     */
	    @SideOnly(Side.CLIENT)
	    public void getSubBlocks(Item itemIn, CreativeTabs tab, List<ItemStack> list) {
	        for (EnumType enumtype : EnumType.values()) {
	            list.add(new ItemStack(itemIn, 1, enumtype.getMetadata()));
	        }
	    }

	    /**
	     * Convert the given metadata into a BlockState for this Block
	     */
	    public IBlockState getStateFromMeta(int meta) {
	        return this.getDefaultState().withProperty(VARIANT, EnumType.byMetadata(meta));
	    }

	    /**
	     * Get the MapColor for this Block and the given BlockState
	     */
	    public MapColor getMapColor(IBlockState state) {
	        return ((EnumType)state.getValue(VARIANT)).func_181070_c();
	    }

	    /**
	     * Convert the BlockState into the correct metadata value
	     */
	    public int getMetaFromState(IBlockState state) {
	        return ((EnumType)state.getValue(VARIANT)).getMetadata();
	    }

	    protected BlockState createBlockState() {
	        return new BlockState(this, new IProperty[] {VARIANT});
	    }

	    public static enum EnumType implements IStringSerializable {
	        OAK(0, "oak", MapColor.woodColor),
	        SPRUCE(1, "spruce", MapColor.obsidianColor),
	        BIRCH(2, "birch", MapColor.sandColor),
	        JUNGLE(3, "jungle", MapColor.dirtColor),
	        ACACIA(4, "acacia", MapColor.adobeColor),
	        DARK_OAK(5, "dark_oak", "big_oak", MapColor.brownColor);

	        private static final EnumType[] META_LOOKUP = new EnumType[values().length];
	        private final int meta;
	        private final String name;
	        private final String unlocalizedName;
	        private final MapColor field_181071_k;

	        private EnumType(int p_i46388_3_, String p_i46388_4_, MapColor p_i46388_5_) {
	            this(p_i46388_3_, p_i46388_4_, p_i46388_4_, p_i46388_5_);
	        }

	        private EnumType(int p_i46389_3_, String p_i46389_4_, String p_i46389_5_, MapColor p_i46389_6_) {
	            this.meta = p_i46389_3_;
	            this.name = p_i46389_4_;
	            this.unlocalizedName = p_i46389_5_;
	            this.field_181071_k = p_i46389_6_;
	        }

	        public int getMetadata() {
	            return this.meta;
	        }

	        public MapColor func_181070_c() {
	            return this.field_181071_k;
	        }

	        public String toString() {
	            return this.name;
	        }

	        public static EnumType byMetadata(int meta) {
	            if (meta < 0 || meta >= META_LOOKUP.length)
	            {
	                meta = 0;
	            }

	            return META_LOOKUP[meta];
	        }

	        public String getName() {
	            return this.name;
	        }

	        public String getUnlocalizedName() {
	            return this.unlocalizedName;
	        }

	        static {
	            for (EnumType enumtype : values()) {
	                META_LOOKUP[enumtype.getMetadata()] = enumtype;
	            }
	        }
	    }   
}
