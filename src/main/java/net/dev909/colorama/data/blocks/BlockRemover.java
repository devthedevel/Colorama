package net.dev909.colorama.data.blocks;

import java.util.List;
import java.util.Random;

import net.dev909.colorama.data.ColoramaReg;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumWorldBlockLayer;		
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockRemover extends Block {

	public BlockRemover(Material materialIn, String name) {
		super(materialIn);
		this.setUnlocalizedName(name);
		this.setStepSound(soundTypeMetal);
		this.setHardness(0.5F);
		this.setResistance(5.0F);
		this.setCreativeTab(ColoramaReg.cTab);
		this.setBlockBounds(0.25F, 0.0F, 0.25F, 0.75F, 0.6F, 0.75F);
		this.setDefaultState(this.blockState.getBaseState().withProperty(BlockPaintcan.VOLUME, 4));
	}
	
	@Override
    public boolean isFullBlock() {return false;}
	
	@Override
	public boolean isOpaqueCube() {return false;}
	
	@SideOnly(Side.CLIENT)
	public EnumWorldBlockLayer getBlockLayer() {return EnumWorldBlockLayer.SOLID;}
	
	@Override
    public AxisAlignedBB getCollisionBoundingBox(World worldIn, BlockPos pos, IBlockState state) {
        return new AxisAlignedBB((double)pos.getX() + this.minX, (double)pos.getY() + this.minY, (double)pos.getZ() + this.minZ, (double)pos.getX() + this.maxX, (double)pos.getY() + this.maxY, (double)pos.getZ() + this.maxZ);
    }
	
	@Override
	public int damageDropped(IBlockState state) {
		return state.getValue(BlockPaintcan.VOLUME).intValue();
	}
	
	@Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		if (state.getValue(BlockPaintcan.VOLUME).intValue() == 0) {
			return ColoramaReg.paintcanEmpty;
		}
		else {
	        return Item.getItemFromBlock(this);	
		}
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item itemIn, CreativeTabs tab, List list) {
		list.add(new ItemStack(itemIn, 1, BlockPaintcan.VOLUME.getAllowedValues().size()-1));
	}
	  
	@Override
	public IBlockState getStateFromMeta(int meta) {
		return this.getDefaultState().withProperty(BlockPaintcan.VOLUME, Integer.valueOf(meta));
	}
	  
	public int getMetaFromState(IBlockState state) {
		return state.getValue(BlockPaintcan.VOLUME).intValue();
	}
	  
	@Override
	protected BlockState createBlockState() {
		return new BlockState(this,new IProperty[] {BlockPaintcan.VOLUME});
	}

}