package net.dev909.colorama.data.blocks;

import net.dev909.colorama.data.ColoramaReg;
import net.dev909.colorama.data.tileentities.CBlock_TE;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CBlock extends Block implements ITileEntityProvider {

	
	public CBlock(Material material, MapColor mapColor, String name) {
		super(material, mapColor);
		this.setUnlocalizedName(name);
	}
	
	public CBlock(Material material, String name) {
		super(material);
		this.setUnlocalizedName(name);
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new CBlock_TE();
	}
	
	@Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }
    
    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
    	super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
    }

	@Override
	public boolean isFullCube() {return true;}
	
	@Override
	public boolean isOpaqueCube() {return true;}
	
	@Override
	public int getRenderType() {return 3;}
	
	@SideOnly(Side.CLIENT)
	public EnumWorldBlockLayer getBlockLayer() {
	    return EnumWorldBlockLayer.SOLID;
    }
	
    @SideOnly(Side.CLIENT)
    public int getBlockColor()
    {
        return 16777215;
    }

    @SideOnly(Side.CLIENT)
    public int getRenderColor(IBlockState state)
    {
        return 16777215;
    }

    @SideOnly(Side.CLIENT)
    public int colorMultiplier(IBlockAccess worldIn, BlockPos pos, int renderPass) {
    	TileEntity te = worldIn.getTileEntity(pos);
	    if (te instanceof CBlock_TE) {
	        return ((CBlock_TE) te).getColor();
	    }
	   return super.colorMultiplier(worldIn, pos, renderPass);
    }

}


